# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
    def link_to_remove_fields(name, f)
        f.hidden_field(:_destroy) + link_to("Remove", '#', onclick: "remove_fields(this); return false;", class: "nav_link")
    end

    def link_to_add_fields(name, f, association)  
        new_object = f.object.class.reflect_on_association(association).klass.new  
        fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|  
          render(association.to_s.singularize + "_fields", :f => builder)  
        end  
        link_to name, "#", :onclick => h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\"); return false;")
    end  
end