class Contact < ApplicationRecord
    has_many :numbers, :dependent => :destroy
    accepts_nested_attributes_for :numbers, :allow_destroy => true
    validates :name, :presence => {message: "Name cannot be blank."}
    validates :user_id, :presence => true
   
end
