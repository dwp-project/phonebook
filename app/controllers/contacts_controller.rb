class ContactsController < ApplicationController
    before_action :authenticate_user!
    
    def index
        if current_user.role == "admin"
            @contact = Contact.all
        else
            @contact = Contact.where(user_id: current_user.id).order(created_at: :desc)
        end
    end

    def new
        @contact = Contact.new
        @contact.numbers.build
    end

    def edit
        @contact = Contact.find(params[:id])
    end

    def create
        @contact = Contact.new(contact_params)
        
        respond_to do |format|
            
            if @contact.save
                format.js
                redirect_to contacts_path
            else
                format.js
               
                
            end
        end
    end

    def update
        @contact = Contact.find(params[:id])
        # if @phonebook.update(contact_params)
            
        #     redirect_to contacts_path
        # else
        #     render 'edit'
        # end

        respond_to do |format|
            
            if @contact.update(contact_params)
                format.js { redirect_to contacts_path }
            else
                format.js
                
                #render 'edit'
            end
        end
    end

    def show
        @contact = Contact.find(params[:id])
    end

    def destroy
        @contact = Contact.find(params[:id])
        @contact.destroy
     
        redirect_to contacts_path
    end

    private
    def contact_params
        params.require(:contact).permit(:user_id, :name, numbers_attributes: [:id, :contact_id, :number, :_destroy])
    end
end

