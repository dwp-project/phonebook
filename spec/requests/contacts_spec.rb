require "rails_helper"

RSpec.describe "Contacts Views", :type => :request do

  it "Call index page" do
    user_sign_in
    get "/contacts/"
    expect(response).to have_http_status 200
  end

  it "Call new page" do
    user_sign_in
    get "/contacts/new"
    expect(response).to have_http_status 200
  end

  it "Call edit page" do
    user_sign_in
    contact = create(:contact)
    get "/contacts/#{contact.id}/edit"
    expect(response).to have_http_status 200
  end

  it "Call sign in page" do
    get "/users/sign_in/"
    expect(response).to have_http_status 200
  end

  it "Call sign up page" do
    get "/users/sign_up/"
    expect(response).to have_http_status 200
  end
end

def user_sign_in
  @user = create(:user)
  sign_in @user
end