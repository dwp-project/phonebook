require 'rails_helper'
require 'faker'


RSpec.describe ContactsController, type: :controller do
    
    before(:each) do
        @user = create(:user)
        sign_in @user
        @contact = create(:contact)
    end

    describe "INDEX 'contacts/index'" do
        
        it "Get user's role" do
            roles = User.roles
            expect(roles).not_to be_nil
        end

        it "Get All Contact" do
            contact = Contact.all
            expect(contact).to be_truthy
        end
    end

    describe "NEW 'contacts/new'" do
        it "Create Contact Object" do
            contact = Contact.new
            expect(contact).to be_truthy
        end
    end

    describe "PUT 'contacts/update/:id'" do
        it "allows an phone to be updated" do
            put :update, xhr: true, params: { id: @contact.id , :contact => @contact.attributes = { :user_id => @contact.user_id, :name => "Home" } }
            expect(response).to have_http_status 200
        end
    end

    describe "DELETE 'contacts/:id'" do
        it "delete contacts" do
            delete :destroy, xhr: false, params: { id: @contact.id }
            expect(response).to have_http_status 302
        end
    end
    
end
