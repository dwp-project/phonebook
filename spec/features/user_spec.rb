require 'rails_helper'
require 'faker'

feature "User log in" do
  
  scenario "with correct email & password" do
    @user = create(:user)
    visit '/users/sign_in'
    within("#new_user") do
      fill_in 'Email', with: @user.email
      fill_in 'Password', with: @user.password
    end
    
    click_button 'Log in'
    expect(page).to have_content 'Phonebook'
  end

  scenario "with wrong email or password" do
    
    wrong_email = Faker::Internet.email
    wrong_password = Faker::Internet.password

    visit '/users/sign_in'
    within("#new_user") do
      fill_in 'Email', with: wrong_email
      fill_in 'Password', with: wrong_password
    end
    
    click_button 'Log in'
    expect(page).to have_content 'Log in'
    expect(page).to have_content 'Email'
    expect(page).to have_content 'Password'
  end
end

feature "User sign up" do
  scenario "with correct email & password & user role" do
    
    email = Faker::Internet.email
    password = Faker::Internet.password

    visit '/users/sign_up'
    within("#new_user") do
      fill_in 'Email', with: email
      fill_in 'Password', with: password
      fill_in 'Password confirmation', with: password
      select  'user', from: 'Role'
    end
    
    click_button 'Sign up'
    expect(page).to have_content 'Phonebook'
  end

  scenario "with correct email & password & admin role" do
    
    email = Faker::Internet.email
    password = Faker::Internet.password

    visit '/users/sign_up'
    within("#new_user") do
      fill_in 'Email', with: email
      fill_in 'Password', with: password
      fill_in 'Password confirmation', with: password
      select  'admin', from: 'Role'
    end

    click_button 'Sign up'
    expect(page).to have_content 'Phonebook'
  end
end