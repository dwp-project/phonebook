require 'faker'

FactoryBot.define do

    factory :contact do
        user_id Faker::Number.digit
        name Faker::Name.name
    end

    factory :number do
        contact_id Faker::Number.digit
        number Faker::Number.number(10)
    end

    factory :user do
        email Faker::Internet.email
        password Faker::Internet.password
    end

  

end
