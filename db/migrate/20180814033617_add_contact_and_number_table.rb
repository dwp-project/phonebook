class AddContactAndNumberTable < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end

    create_table :numbers do |t|
      t.integer :contact_id
      t.string :number

      t.timestamps
    end
  end
end
